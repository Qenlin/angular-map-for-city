package com.axionis.findhome;

import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.SquareCap;

import java.util.Random;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    public static final double WIDTH_OF_SQUARE = 0.002;
    public static final double HEIGHT_OF_SQUARE = 0.004;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we add a marker near Płock, Poland and in random way color the city.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Random generator = new Random();

        // Add a marker in Płock and move the camera
        LatLng sydney = new LatLng(52.54682, 19.70638);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Płock"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.moveCamera(CameraUpdateFactory.zoomTo(13));

        // Draw square with random colors (green or red with random alpha) on whole Płock
        double xStart = 52.51682;
        double yStart = 19.67638;
        for (int x = 0; x < 20; x++) {
            for (int y = 0; y < 20; y++) {

                int alpha = generator.nextInt(200) - 100;

                if (alpha > 0) {
                    showSquare(xStart + (WIDTH_OF_SQUARE * x), yStart + (HEIGHT_OF_SQUARE * y), getGreenWithAlpha(alpha));
                } else if (alpha < 0) {
                    showSquare(xStart + (WIDTH_OF_SQUARE * x), yStart + (HEIGHT_OF_SQUARE * y), getRedWithAlpha(Math.abs(alpha)));
                }
            }
        }
    }

    private int getGreenWithAlpha(int alpha) {
        return Color.argb(alpha, 101, 236, 67);
    }

    private int getRedWithAlpha(int alpha) {
        return Color.argb(alpha, 233, 83, 83);
    }

    void showSquare(Double xCenter, Double yCenter, int color) {
        PolygonOptions rectOptions = new PolygonOptions()
                .add(new LatLng(xCenter + WIDTH_OF_SQUARE / 2, yCenter + HEIGHT_OF_SQUARE / 2),
                        new LatLng(xCenter - WIDTH_OF_SQUARE / 2, yCenter + HEIGHT_OF_SQUARE / 2),
                        new LatLng(xCenter - WIDTH_OF_SQUARE / 2, yCenter - HEIGHT_OF_SQUARE / 2),
                        new LatLng(xCenter + WIDTH_OF_SQUARE / 2, yCenter - HEIGHT_OF_SQUARE / 2),
                        new LatLng(xCenter + WIDTH_OF_SQUARE / 2, yCenter + HEIGHT_OF_SQUARE / 2))
                .fillColor(color)
                .strokeWidth(0)
                .zIndex(55);

        mMap.addPolygon(rectOptions);
    }

    private void showCirclesInCircle(LatLng latLng, int color) {
        Circle cir = mMap.addCircle(new CircleOptions().center(latLng)
                .radius(150)
                .strokeWidth(0)
                .fillColor(color)
                .zIndex(55));
        cir.setVisible(true);
        Circle cir2 = mMap.addCircle(new CircleOptions().center(latLng)
                .radius(200)
                .strokeWidth(0)
                .fillColor(color)
                .zIndex(55));
        cir2.setVisible(true);
        Circle cir3 = mMap.addCircle(new CircleOptions().center(latLng)
                .radius(250)
                .strokeWidth(0)
                .fillColor(color)
                .zIndex(55));
        cir3.setVisible(true);
    }

}
